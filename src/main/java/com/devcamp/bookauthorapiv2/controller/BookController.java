package com.devcamp.bookauthorapiv2.controller;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapiv2.model.Book;
import com.devcamp.bookauthorapiv2.service.BookService;

@CrossOrigin   //Java @CrossOrigin: cho phép CORS trên RESTful web service.
@RestController
public class BookController {  

    @Autowired
    private BookService bookService;

    //http://localhost:8080/books
    @GetMapping("/books")
    public ArrayList<Book> splitString() {
        ArrayList<Book> arrayListBooks = bookService.getBookList();
        // trả về là arr list
        return arrayListBooks;
    }

    @GetMapping("/book-quantity")
    public ArrayList<Book> filterQTYBock(@RequestParam (name="qty" , required = true) int qty) {
        ArrayList<Book> arrayListBooks = bookService.getfilterQTYBook(qty);
        
        // trả về là arr list
        return arrayListBooks;
    }

    @GetMapping("books/{bookId}")
    public Book getBookIndex(@PathVariable (name="bookId" , required = true) int paramIndex) {
        return bookService.getBookIndex(paramIndex);
    }

    
}
