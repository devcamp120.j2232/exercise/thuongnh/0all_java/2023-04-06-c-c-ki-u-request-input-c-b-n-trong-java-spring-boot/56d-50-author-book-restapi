package com.devcamp.bookauthorapiv2.controller;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapiv2.model.Author;
import com.devcamp.bookauthorapiv2.service.AuthorService;

@CrossOrigin
@RestController
public class AuthorController {

    @Autowired 
    private AuthorService authorService;

    @GetMapping("/author-info")
    public Author getAuthorForEmail(@RequestParam(name="email",defaultValue = "") String paramEmail){
        Author author = authorService.getfilterAuthorForEmail(paramEmail);
        return author;
    }
    // ruyền vào param gender từ request, có đầu ra là ArrayList<Author>, thực hiện trả ra danh sách Author có gender tương ứng.

    @GetMapping("/author-gender")
    public ArrayList<Author> getAuthorForGender(@RequestParam(name="gender",required = true) char paramGender){
        ArrayList<Author> author = authorService.getfilterAuthorForGender(paramGender);
        return author;
    }

    

}
