package com.devcamp.bookauthorapiv2.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.bookauthorapiv2.model.Author;
import com.devcamp.bookauthorapiv2.model.Book;

@Service
public class BookService {

    @Autowired
    private AuthorService authorService;

    // phương thức trả về tất cả Arrlist
    public ArrayList<Book> getBookList() {
        // khởi tạo 3 đối tượng sách truyền vào là Arr lấy từ authorService
        ArrayList<Author> arrAuthor1 = authorService.getArrayAuthor1();
        ArrayList<Author> arrAuthor2 = authorService.getArrayAuthor2();
        ArrayList<Author> arrAuthor3 = authorService.getArrayAuthor3();
        // add Array author vào đối tượng sách
        Book book1 = new Book("toan", arrAuthor1, 1000, 5);
        Book book2 = new Book("ly", arrAuthor2, 2000, 6);
        Book book3 = new Book("hoa", arrAuthor3, 3000, 1);

        ArrayList<Book> listString = new ArrayList<Book>();
        // thêm sách vào arr list
        listString.add(book1);
        listString.add(book2);
        listString.add(book3);
        return listString;
    }

    // thực hiện trả ra danh sách Book có quantity lớn hơn hoặc bằng quantityNumber
    public ArrayList<Book> getfilterQTYBook(int quantityNumber) {
        ArrayList<Book> arrListBook = getBookList();
        // khai báo 1 mảng rổng
        ArrayList<Book> arrListBookRong = new ArrayList<Book>();
        for (Book bookElement : arrListBook) {
            if (bookElement.getQty() > quantityNumber) {
                arrListBookRong.add(bookElement);
            }
        }
        return arrListBookRong;
    }

    // có đầu ra là Book thực hiện trả ra Book có vị trí thứ index trong danh sách
    // đã khai báo
    public Book getBookIndex(int paramIndex) {
        ArrayList<Book> arrListBook = getBookList();
        return arrListBook.get(paramIndex);
    }

}
